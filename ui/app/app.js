(function () {
    'use strict';


    var app = angular
        .module('app', ['ui.router', 'ngCookies', 'angular-google-gapi'])
        .factory('XSRFInterceptor',['$cookies', function ($cookies) {
            var XSRFInterceptor = {
                request: function (config) {
                    var token = $cookies.get('XSRF-TOKEN');
                    console.log($cookies.getAll());
                    if (token) {
                        config.headers['X-XSRF-TOKEN'] = token;
                    }
                    return config;
                }
            };
            return XSRFInterceptor;
        }])
        .config(['$httpProvider', function ($httpProvider) {
            $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
            $httpProvider.defaults.withCredentials = true;
            $httpProvider.interceptors.push('XSRFInterceptor');
        }]);
})();
