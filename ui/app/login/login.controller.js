(function() {
  'use strict';

  angular
    .module('app')
    .controller('LoginController', LoginController);

  function LoginController($http, $state, authService, profileService){
    var vm = this;

    vm.login = login;
    vm.googleLogin = googleLogin
    vm.username = '';
    vm.password = '';

    function login(){
      authService.login(vm.username,vm.password).then(function(response){
        profileService.profile = response;
        $state.go("profile");
      }, function(error){
        vm.message = error;
      });
    }

    function googleLogin(){
      authService.googleLogin().then(function(response){
        profileService.profile = response;
        $state.go("profile");
      }, function(error){
        vm.message = error;
      });
    }
  }

})();
