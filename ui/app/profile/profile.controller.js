(function() {
  'use strict';

  angular
    .module('app')
    .controller('ProfileController', ProfileController);

  function ProfileController($state, profileService){
    var vm = this;

    vm.logout = logout;
    vm.profile = {};

    init();

    ////
    function init(){
      vm.profile = profileService.profile;
    }

    function logout(){
        profileService.logout().then(function(success){
        $state.go("login");
      },function(error){
        console.log(error);
      });
    }
  }

})();
