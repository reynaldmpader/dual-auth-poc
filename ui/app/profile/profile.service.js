(function() {
  'use strict';

  angular
  .module('app')
  .service('profileService', profileService);

  function profileService($q, authService){
    var service = this;

    service.profile = {};
    service.logout = logout;

    function logout(){
      var deferred = $q.defer();
      $q.when(authService.logout()).then(function(success){
        console.log("Log out success!");
        service.profile = {};
        deferred.resolve();
      },function(error){
        deferred.reject(error);
      });
      return deferred.promise;
    }
  }

})();
