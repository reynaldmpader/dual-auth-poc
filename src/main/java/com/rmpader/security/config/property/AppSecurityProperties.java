package com.rmpader.security.config.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * @author RMPader
 */
@Component
@ConfigurationProperties(prefix = "app.security")
public class AppSecurityProperties {

    @NotNull
    private String googleLoginUrl;

    @NotNull
    private String googleClientId;

    @NotNull
    private String defaultLoginUrl;

    @NotNull
    private String successfulAuthRedirectUrl;

    public String getGoogleLoginUrl() {
        return googleLoginUrl;
    }

    public String getGoogleClientId() {
        return googleClientId;
    }

    public String getDefaultLoginUrl() {
        return defaultLoginUrl;
    }

    public String getSuccessfulAuthRedirectUrl() {
        return successfulAuthRedirectUrl;
    }

    public void setGoogleLoginUrl(String googleLoginUrl) {
        this.googleLoginUrl = googleLoginUrl;
    }

    public void setGoogleClientId(String googleClientId) {
        this.googleClientId = googleClientId;
    }

    public void setDefaultLoginUrl(String defaultLoginUrl) {
        this.defaultLoginUrl = defaultLoginUrl;
    }

    public void setSuccessfulAuthRedirectUrl(String successfulAuthRedirectUrl) {
        this.successfulAuthRedirectUrl = successfulAuthRedirectUrl;
    }
}
