package com.rmpader.security.config;

import com.rmpader.security.config.property.AppSecurityProperties;
import com.rmpader.security.data.repository.UserAuthenticationRepository;
import com.rmpader.security.http.AuthenticationHandler;
import com.rmpader.security.http.CSRFFilter;
import com.rmpader.security.util.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

/**
 * @author RMPader
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserAuthenticationRepository userAuthenticationRepository;

    @Autowired
    private AppSecurityProperties securityProperties;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        AuthenticationHandler handler = new AuthenticationHandler();

        GoogleAuthenticationFilter filter = new GoogleAuthenticationFilter(securityProperties,
                                                                           userAuthenticationRepository);
        filter.setAuthenticationSuccessHandler(handler);

        // @formatter:off
        http.authorizeRequests()
                .antMatchers(securityProperties.getSuccessfulAuthRedirectUrl(),
                             securityProperties.getGoogleLoginUrl(),
                             securityProperties.getDefaultLoginUrl()).permitAll()
                .anyRequest().authenticated()
            .and()
                .exceptionHandling()
                    .authenticationEntryPoint(new Http403ForbiddenEntryPoint())
            .and()
                .formLogin()
                    .successHandler(handler)
                    .failureHandler(handler)
            .and()
                .logout()
                    .logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler(HttpStatus.OK))
            .and()
                .addFilterBefore(filter,UsernamePasswordAuthenticationFilter.class)
                .addFilterAfter(new CSRFFilter(), CsrfFilter.class)
                .csrf()
                    .csrfTokenRepository(csrfTokenRepository());
        // @formatter:on
    }

    @Bean
    public CsrfTokenRepository csrfTokenRepository() {
        HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
        repository.setHeaderName("X-XSRF-TOKEN");
        return repository;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authenticationManagerBuilder,
                                UserDetailsServiceImpl userDetailsService,
                                PasswordEncoder passwordEncoder,
                                MessageSource messageSource) throws Exception {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setPasswordEncoder(passwordEncoder);
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setMessageSource(messageSource);
        authenticationManagerBuilder.authenticationProvider(authenticationProvider);
        authenticationManagerBuilder.userDetailsService(userDetailsService);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
